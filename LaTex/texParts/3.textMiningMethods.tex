\section{Przetwarzanie dokumentów tekstowych}\label{textMiningMethods}

Większość dokumentów tekstowych znajduje się w postaci nieustrukturyzowanej, czyli takiej która jest przyjazna dla człowieka, a nie dla komputera. To, co jest niezbędne do zrozumienia zdania człowiekowi, np. przecinki, zaimki czy pełne formy wyrazów, staje się zbędne dla komputera. Istnieje potrzeba wykorzystania algorytmów eksploracji danych do analizy dokumentów tekstowych, ale najpierw należy je poddać przetwarzaniu (ang. \textit{preprocessing}).

\par
Przebieg preprocessingu zależy od konkretnego zadania, jednak składa się on z kilku standardowych etapów. Do każdej z poniżej opisanych metod oczyszczania tekstu zostanie przedstawiony konkretny przykład na podstawie zdania: \textit{\blockquote{To były już pięćdziesiąte urodziny Mateusza, ale wciąż pozostało mu wiele krajów do odwiedzenia, aby spełnić swoje marzenie.}}. Każdy poniższy przykład jest przedstawiony na niezmienionym zdaniu, tzn. bez użycia innych metod przetwarzania tekstu w tym samym czasie.

\subsection{Tokenizacja}
Tokenizacja \cite{preprocessing} opisuje ogólny proces rozbijania tekstu dokumentu na poszczególne elementy, które następnie są wykorzystywane w różnych algorytmach przetwarzania języka naturalnego. Zwykle tokenizacji towarzyszą inne, opcjonalne operacje mające na celu przygotowanie tekstu do algorytmów. Zabieg polega na podzieleniu łańcuchów wyrazów na części takie jak słowa, słowa kluczowe, frazy, symbole i inne elementy zwane tokenami. Tokeny mogą być pojedynczymi słowami lub całymi zdaniami. Podczas procesu tokenizacji niektóre składowe takie jak znaki interpunkcyjne lub znaki specjalne są odrzucane. 

\par
Nawiązując do przykładu przedstawionego we wstępie niniejszego rozdziału, poniżej wynik zastosowania tokenizacji na tamtym zdaniu:\\
\smallbreak
\textit{To, były, już, pięćdziesiąte, urodziny, Mateusza, ale, wciąż, pozostało, mu, wiele, krajów, do, odwiedzenia, aby, spełnić, swoje, marzenie.}

\subsection{Usuwanie nieznaczących słów przy użyciu stop listy} \label{stopListDescription}
Stop lista \cite{preprocessing}, to zbiór słów które pomimo tego, że znalazły się w danym wyrażeniu to ich obecność nie wpływa znacząco na ostateczną informację wynikającą z treści zdania. Są to często występujące części zdania. W języku polskim takie słowa, to np.: \blockquote{być}, \blockquote{do}, \blockquote{bo}, \blockquote{lub}, \blockquote{i}, \blockquote{a}, itd. 

\par
Nawiązując do przykładu przedstawionego we wstępie niniejszego rozdziału, poniżej wynik zastosowania stop listy na tamtym zdaniu:\\
\smallbreak
\textit{pięćdziesiąte, urodziny, Mateusza, pozostało, wiele, krajów, odwiedzenia, spełnić, swoje, marzenia. \\}
\smallbreak

\par
W celu osiągnięcia efektu jak na przykładzie należy zdecydować się na jedno z podejść:

\begin{enumerate}
	\item Przeszukanie słownika w celu znalezienia specyficznych dla języka słów i stworzenia z nich stop listy. Następnie należy dokonać operacji filtrowania tekstu w dokumencie i odrzucić wyrażenia, które występują w liście zbędnych słów.
	\item Alternatywnym podejściem jest przygotowanie stop listy poprzez posortowanie wszystkich słów w dokumencie według częstości występowania. Taki zbiór słów po zostawieniu w nim tylko niepowtarzających się wyrażeń może zostać użyty do usunięcia wszystkich słów, które występują najczęściej w stop liście.
\end{enumerate} 

\subsection{Stemming}
Stemming \cite{preprocessing} to proces sprowadzenia słowa do jego rdzenia. Polega na wydobyciu z wybranego wyrazu tzw. rdzenia, a więc tej jego części, która jest odporna na odmiany przez przyimki, rodzaje itp. Po zastosowaniu stemmingu otrzymuje się ciąg liter, który nie jest pełnoprawnym wyrazem, ale częścią wspólną niezmieniającą się w czasie odmiany. Powstały ciąg znaków zwiększa popularność danego słowa, pod warunkiem że jest przeprowadzony prawidłowo. W przypadku gdy stemming wykonano niewłaściwie pojawiają się problemy, podobnie jak przy lematyzacji (sekcja \ref{lemmatization}). Przykładem może być słowo z języka angielskiego \blockquote{saw}, które poddane stemmingowi zwróci jeden znak \blockquote{s}. 

\newpage

\par
Nawiązując do przykładu przedstawionego we wstępie niniejszego rozdziału, poniżej wynik zastosowania stemmingu na tamtym zdaniu: \\

\smallbreak
\textit{to, by, już, pięćdziesiąt, urodzin, Mateusz, ale, wciąż, pozosta, mu, wiel, kraj, do, odwiedz, aby, spełni, swoje, marzeni.}

\subsection{Lematyzacja} \label{lemmatization}
Lematyzacja \cite{preprocessing} jest procesem, w którym wyrazy są sprowadzane do formy bezokolicznika. Celem jest zmniejszenie liczby różnych słów. Jeżeli operacja wykonana jest prawidłowo, to tekst staje się bardziej ogólny co pozytywnie odbija się w późniejszej klasyfikacji. Natomiast w niektórych przypadkach, np. w języku angielskim słowo \blockquote{saw} zależnie od tego czy w danym przypadku jest czasownikiem czy rzeczownikiem ma inne znaczenie, a także formę bezokolicznika (\blockquote{see} lub \blockquote{saw}). Częściowym rozwiązaniem problemu może być niepełna lematyzacja polegająca na sprowadzeniu słów do ich form bezokolicznikowych pod warunkiem, że aktualna forma słowa jest unikalna i nie ma innego znaczenia - nie tak jak w przypadku wyrażenia \blockquote{saw}. Lematyzacja obliczeniowo jest trudniejsza i kosztowniejsza niż stemming i w praktyce oba wspomniane zabiegi mają niewielki wpływ na skuteczność klasyfikacji tekstu \cite{lemmatizationAndStemming}.

\par
Nawiązując do przykładu przedstawionego we wstępie niniejszego rozdziału, poniżej wynik zastosowania lematyzacji na tamtym zdaniu: \\

\smallbreak
\textit{To, być, już, pięćdziesiąt, urodziny, Mateusz, ale, wciąż, pozostać, on, wiele, kraj, do, odwiedzić, aby, spełnić, swoje, marzenie.}

\subsection{Model worka słów} \label{bagOfWordsModel}
Przed dopasowaniem modelu i wykorzystaniem algorytmów uczenia maszynowego do treningu, należy zastanowić się jak najlepiej przedstawić dokument tekstowy jako wektor cech. Powszechnie stosowanym modelem w przetwarzaniu języka naturalnego (\textit{ang. Natural Language Processing}) jest tzw. model worka słów (\textit{ang. Bag of words}) \cite{bagOfWordsModel}. Idea tego modelu jest prosta jak jego nazwa. W pierwszej kolejności powstaje słownik - zbiór wszystkich, różnych słów, które występują w zbiorze treningowym, a każde słowo jest powiązane z liczbą jego wystąpień w tekście. Utworzony słownik może być rozumiany jako zbiór niepowtarzalnych słów, gdzie kolejność nie ma znaczenia. Niech \(D_1 \) i  \( D_2 \) będą dwoma dokumentami w zbiorze treningowym:

\begin{itemize}
	\item \( D_1: \) \blockquote{Woda jest źródłem życia.}
	\item \( D_2: \) \blockquote{Podstawą życia człowieka jest woda.}
\end{itemize}

Na podstawie powyższych dokumentów, słownik może zostać zapisany jako:

\[ V = { woda:2, jest:2, zrodlem:1, zycia:2, podstawa:1, czlowieka:1 } \] \[ \]

\begin{table}[ht]\footnotesize
\begin{center}
\caption{Reprezentacja dwóch przykładowych dokumentów w modelu worka słów}
\label{table:bagOfWordsExample}
\begin{tabular}{ l l l l l l l }
\hline
 & woda & jest & źródłem & życia & podstawą & człowieka \\ \hline
\( x_{D_1} \) & 1 & 1 & 1 & 1 & 0 & 0 \\
\( x_{D_2} \) & 1 & 1 & 0 & 1 & 1 & 1 \\
\( \sum \) & 2 & 2 & 1 & 2 & 1 & 1 \\
\hline
\end{tabular}
\caption*{źródło: opracowanie własne}
\end{center}
\end{table}

Podczas analizy przykładu przedstawionego w tabeli \ref{table:bagOfWordsExample} może pojawić się pytanie - czy wartości 0 i 1 są liczbami binarnymi (1 jeśli słowo występuje w dokumencie, 0 w przeciwnym wypadku) czy liczbą wystąpień w danym dokumencie. Odpowiedź zależy od tego, który model probabilistyczny zostanie użyty dla naiwnego klasyfikatora Bayesa: model Bernouliego czy wielomianowy - więcej na ten temat w rozdziale czwartym. Warto również dodać, że pozycja poszczególnych słów w modelu worka słów nie ma znaczenia. Oznacza to, że nieważne czy słowo znajduje się na początku, czy na końcu całego zbioru, to i tak traktowane jest w taki sam sposób, niezależnie od pozycji.

\subsection{N-gramy}
W modelu \textit{n}-gramowym \cite{bagOfWordsModel} token może być zdefiniowany jako ciąg \textit{n} słów. Najprostszy przykład to tzw. unigram (1-gram), gdzie każde słowo składa się z jednego słowa, litery lub symbolu. Wszystkie poprzednie przykłady były oparte o unigramy i taki model będzie także używany w niniejszej pracy dyplomowej. Poniżej przykłady różnych n-gramów dla zdania \blockquote{Wiosną rośliny budzą się do życia.}:

\begin{itemize}
	\item unigram (1-gram): \\
		\begin{tabular}{ |c|c|c|c|c|c| }
			\hline
			wiosną & rośliny & budzą & się & do & życia \\
			\hline
		\end{tabular}
	\item bigram (2-gram): \\
		\begin{tabular}{ |c|c|c|c|c| }
			\hline
			wiosną rośliny & rośliny budzą & budzą się & się do & ... \\
			\hline
		\end{tabular}
	\item trigram (3-gram): \\
		\begin{tabular}{ |c|c|c|c| }
			\hline
			wiosną rośliny budzą & rośliny budzą się & budzą się do & ... \\
			\hline
		\end{tabular}
\end{itemize}

\subsection{Podsumowanie}
Wszystkie wyżej wymienione zabiegi stosuje się w celu ulepszenia przeprowadzanej analizy dokumentów tekstowych oraz ich wydajniejszego indeksowania. Zabiegi te stosowane w kontekście analizy tekstu pozwalają na identyfikację początkowego zestawu cech, który może być później ograniczony (i zoptymalizowany) w procesie wydobywania wyrażeń.