\section{Naiwny klasyfikator Bayesa} \label{bayes}

Pół wieku temu naukowcy zastanawiali się czy możliwe będzie zbudowanie modelu, który będzie w stanie uczyć się na podstawie dostępnych danych, a następnie automatycznie podejmie dobrą decyzję w swoich przewidywaniach. Patrząc wstecz, brzmi to abstrakcyjnie, a odpowiedź można znaleźć w licznych aplikacjach, które pojawiają się w dziedzinie klasyfikacji wzorów, uczenia maszynowego i sztucznej inteligencji.

\par
Dane z różnych urządzeń detekcyjnych w połączeniu z potężnymi algorytmami uczenia się i wiedzą domenową doprowadziły do wielu wielkich wynalazków, które obecnie przyjmowane są za pewnik w codziennym życiu:

\begin{itemize}
	\item zapytania internetowe za pośrednictwem wyszukiwarek takich jak Google,
	\item ropoznawanie tekstu na poczcie,
	\item skanery kodów kreskowych w supermarkecie,
	\item diagnozowanie chorób,
	\item rozpoznawanie mowy przez Siri lub Google Now na smartfonach.
\end{itemize}

\par
Jedną z gałęzi predykcji jest klasyfikacja z nadzorem. Polega ona na szkoleniu modelu na podstawie etykietowanych danych treningowych, dzięki którym powstaje pewna wiedza umożliwiająca określić etykietę innych, nowych obiektów. Jednym z przykładów, jaki zostanie omówiony będzie filtrowanie wiadomości typu \textit{spam} za pomocą naiwnego klasyfikatora Bayesa \cite{naiveBayesStanord}. Celem będzie przewidywanie czy nową wiadomość tekstową można zakwalifikować jako spam, czy nie-spam. 

\par
Naiwny klasyfikator Bayesa należy do rodziny klasyfikatorów liniowych, które są oparte na popularnym twierdzeniu Bayesa. Słyną z tworzenia prostych, ale dobrze działających modeli znanych ze swojej wydajności, szczególnie w dziedzinie klasyfikacji dokumentów czy diagnozy chorób. Probabilistyczny model klasyfikatora oparty jest na twierdzeniu Bayesa. Wymaga, aby zbiór klas decyzyjnych był skończony i wcześniej zdefiniowany. Przymiotnik \textit{naiwny} wynika z założenia, że cechy w zbiorze danych są wzajemnie niezależne. W rzeczywistości założenie niezależności jest zwykle niemożliwe do spełnienia, ale nawet pomimo tego naiwny klasyfikator Bayesa ma bardzo dobre wyniki w tych nierealistycznych założeniach. \cite{bayesGoodPerformanceUnderNaiveAssumptions}. Szczególnie dla małych próbek ten klasyfikator może przewyższyć mocniejsze alternatywy \cite{bayesGoodPeformanceComparedToOthers}.

\par
Z powodu łatwości implementacji, szybkości i skuteczności naiwny klasyfikator Bayesa używany jest w wielu dziedzinach. Niektóre przykłady obejmują:

\begin{itemize}
	\item diagnozę chorób i podejmowanie decyzji dotyczących procesu leczenia \cite{treatmentProcesses},
	\item klasyfikacja sekwencji RNA w badaniach taksonomicznych \cite{rnaClassification},
	\item filtrowanie spamu w wiadomościach e-mail \cite{spamFiltering}.
\end{itemize}

Jednakże silne naruszenia założeń dotyczących niezależności i nieliniowe problemy klasyfikacji mogą prowadzić do małej skuteczności klasyfikatora. Należy pamiętać, że rodzaj danych, a także typ problemu które trzeba rozwiązać określa jaki model klasyfikacji powinien zostać wybrany. W praktyce zaleca się porównanie różnych modeli klasyfikacji na konkretnym zestawie danych i rozważeniu, który z modeli będzie najbardziej odpowiedni.

\subsection{Prawdopodobieństwa a posteriori} \label{posterioriProbability}
Aby zrozumieć jak działa naiwny klasyfikator Bayesa należy krótko omówić koncepcję zasady Bayesa. Model prawdopodobieństwa został sformułowany przez Thomasa Bayesa, jest dość prosty, ale niezwykle potężny:

\[ P(C_i|X) = \frac{P(X|C_i) * P(C_i)}{P(X)} \] gdzie:
\begin{itemize}
	\item \( X \) - oznacza przykład, dla którego klasa decyzyjna jest nieznana. Jest reprezentowany przez \textit{n}-				wymiarowy wektor, gdzie \textit{n} oznacza liczbę atrybutów danego przykładu, czyli \( X = (X_1, X_2, ..., X_n) 			\)
	\item \( P(C_i) \) - prawdopodobieństwo wystąpienia klasy \( C_i \) (tj. prawdopodobieństwo, że dowolny
		przykład należy do klasy \( C_i \))
	\item \( P(X|C_i) \) - prawdopodobieństwo warunkowe, że X wystąpi przy wylosowaniu klasy \( C_i \)
	\item \( P(X) \) - prawdopodobieństwo \textit{a priori} wystąpienia przykładu X
	\item \( P(C_i|X) \) - prawdopodobieństwo \textit{a posteriori} (wynikowe)
\end{itemize}

\par
Twierdzenie Bayesa jest podstawą całej koncepcji naiwnego klasyfikatora Bayesa. Prawdopodobieństwo \textit{a posteriori} w kontekście problemu klasyfikacji można interpretować jako: "Jakie jest prawdopodobieństwo, że dany obiekt należy do klasy \( i \) biorąc pod uwagę jego cechy?". Bardziej rzeczywistym przykładem może być: "Jakie jest prawdopodobieństwo, że dana osoba cierpi na cukrzycę, biorąc pod uwagę pewną wartość przed pomiarem stężenia glukozy we krwi i pewną wartość dla
pomiaru glukozy po posiłku?"

\[ P(cukrzyca | x_i) , x_i = [85mg/dl, 150mg/dl] \]

Niech

\begin{itemize}
	\item \( x_i \) - będzie wektorem cech próbki i, \( i \in {1, 2, ..., n} \),
	\item \( c_j \) - będzie zapisem klasy j, \( j \in {1, 2, ..., m} \),
	\item i \( P(x_i | c_j) \) - będzie prawdopodobieństwem obserwacji próbki \textbf{\( x_i \)}, zakładając że należy do klasy \( c_j \).
\end{itemize}

Ogólny wzór na policzenie tego prawdopodobieństwa może być zapisany jako: 
\[  
	P(c_j | x_i) = \frac{P(x_i | c_j) * P(c_j)}{P(x_i)}
\]\\

Jako wynik wybierana jest ta klasa, dla której wyliczono największe prawdopodobieństwo na podstawie zbioru treningowego.

\[  
	\textrm{przewidziana klasa decyzyjna} \longleftarrow \argmax_{j = 1, ..., m} P(c_j | x_i)
\]\\

W celu kontynuowania powyższego przykładu można sformułować regułę decyzyjną na podstawie prawdopodobieństw \textit{a posteriori} w następujący sposób:
\begin{center}
jeżeli
\(
P(cukrzyca|x_i) \geq P(zdrowy|x_i),
\) \\
osoba jest chora na cukrzycę, \\
w przeciwnym wypadku osoba jest zdrowa.
\end{center}

\subsection{Reguła decyzyjna w klasyfikacji}

W przypadku klasyfikacji spamu regułę decyzyjną naiwnego klasyfikatora Bayesa opartego na prawdopodobieństwie \textit{a posteriori} można wyrazić jako:

\begin{center}
jeżeli
\(
P(c = \textrm{spam}|x) \geq P(c=\textrm{nie-spam}|x),
\) \\
sklasyfikuj tekst jako spam, \\
w przeciwnym wypadku tekst nie jest spamem.
\end{center}

Zgodnie z tym co napisano w sekcji \ref{posterioriProbability}, a także powyższym przykładem można stwierdzić że wyliczanie mianownika (prawdopodobieństwo \textit{a posteriori}) nie zmieni decyzji klasyfikacji. Jest to spowodowane tym, że za każdym razem to co zostało wyliczone w liczniku, będzie dzielone przez stałą wartość rozmiaru słownika. 

\[ P(c = \textrm{spam}|x) = P(x|c = \textrm{spam}) * P(\textrm{spam}) \]
\[ P(c = \textrm{nie-spam}|x) = P(x|c = \textrm{nie-spam}) * P(\textrm{nie-spam}) \] \[\]

Prawdopodobieństwa \textit{a priori} można uzyskać poprzez oszacowania proporcji wiadomości typu spam i nie-spam w całym zbiorze treningowym:

\[ P(c = \textrm{spam}) = \frac{\textrm{liczba wiadomości typu spam}}{\textrm{liczba wszystkich wiadomości w zbiorze treningowym}} \]
\[ P(c = \textrm{nie-spam}) = \frac{\textrm{liczba wiadomości typu nie-spam}}{\textrm{liczba wszystkich wiadomości w zbiorze treningowym}} \] \[ \]

Zakładając, że słowa w każdym dokumencie są warunkowo niezależne (zgodnie z założeniem \textit{naiwności}) można zastosować dwa różne modele do policzenia prawdopodobieństw: model Bernoulliego (sekcja \ref{bernoulliModel}) lub model wielomianowy (sekcja \ref{multinominalModel}).

\subsection{Wielowymiarowy model Bernoulliego} \label{bernoulliModel}
Wielowymiarowy model Bernoulliego \cite{naiveBayesStanordBernoulli} oparty jest na danych binarnych: każdy token w wektorze cech dokumentu jest powiązany z wartością \textit{0} lub \textit{1}. Wektor cech jest \( m \)-wymiarowy, gdzie \( m \) jest liczbą słów w całym słowniku (sekcja \ref{bagOfWordsModel}). Wartość \textit{1} oznacza, że dane wyrażenie wystąpiło w danym dokumencie, a wartość \textit{0} to brak wystąpienia słowa w dokumencie. Całość można zapisać jako: \\

\begin{center}
\(P(x|c_j) = \displaystyle \prod_{i=1}^m P(x_i|c_j)^b * (1 -P(x_i|c_j))^b \), gdzie \((b \in 0,1) \) \[\]
\end{center}

Niech \( P(x_i|c_j) \) będzie wyliczonym, najwyższym prawdopodobieństwem na to, że dane słowo (lub token) $x_i$ występuje w klasie $c_j$.

\[ P(x_i|w_j) = \frac{df_{x_i,y}+1}{df_y+2} \]gdzie

\begin{itemize}
	\item $df_{x_i,y}$ jest liczbą dokumentów w zbiorze treningowym które zawiera cechę $x_i$ i należy do klasy $c_j$
	\item $df_y$ jest liczbą dokumentów w zbiorze treningowym które należą do klasy $c_j$
	\item $+1$ i $+2$ są parametrami wygładzania Laplace'a (sekcja \ref{laplace})
\end{itemize}

\subsection{Model wielomianowy} \label{multinominalModel}

\subsubsection{Częstość wystąpienia} \label{termFrequency}
Alternatywne podejście, zamiast wykorzystania binarnych wartości w modelu Bernoulliego, jest takie oparte na \textit{częstości wystąpienia} (ang. term frequency) ($tf(t, d)$) \cite{naiveBayesStanord}. Częstość wystąpienia jest zwykle zdefiniowana jako liczba wystąpień danego wyrażenia \textit{t} (np. słowa lub tokenu) w dokumencie \textit{d}. W praktyce częstość wystąpienia jest zwykle normalizowana przez podzielenie liczby wystąpień danego słowa przez liczbę słów w całym dokumencie.

\begin{center}
znormalizowana częstość wystąpienia = $\frac{tf(t,d)}{n_d}$
\end{center}

gdzie

\begin{itemize}
	\item $tf(t, d)$: liczba wystąpień danego wyrażenia \textit{t} w dokumencie \textit{d}
	\item $n_d$: liczba wszystkich wyrażeń w dokumencie \textit{d}	
\end{itemize}

W kolejnym kroku częstość wystąpień może zostać użyta do oszacowania prawdopodobieństwa przynależności do danej klasy $c_j$ na podstawie danych treningowych w modelu wielomianowym:

\[ P(x_i|c_j) = \frac{\sum tf(x_i,d \in c_j) + \alpha}{\sum N_{d \in c_j} + \alpha * V} \]

gdzie

\begin{itemize}
	\item $x_i$: słowo z wektora cech \textit{x} danej próbki
	\item $\sum tf(x_i,d \in c_j)$: suma liczby wystąpień słowa $x_i$ we wszystkich dokumentach w zbiorze treningowym, które należą do klasy decyzyjnej $c_j$
	\item $\sum N_{d \in c_j}$: suma wszystkich słów w zbiorze treningowym dla klasy decyzyjnej $c_j$
	\item $\alpha$: dodatkowy parametr wygładzający ($\alpha = 1$ wygładzanie Laplace'a - wyjaśnione poniżej w sekcji \ref{laplace})
	\item \textit{V}: rozmiar całego słownika (liczba różnych, unikalnych słów w zbiorze treningowym)
\end{itemize}

Prawdopodobieństwo, że dany zbiór cech \textit{x} należy do klasy $c_j$ może być wyliczone poprzez przemnożenie przez siebie prawdopodobieństwa poszczególnych słów (przy założeniu naiwności klasyfikatora Bayesa):

\[ P(x|c_j) = P(x_1|c_j) * P(x_2|c_j) * \dots * P(x_n|c_j) = \prod_{i=1}^m P(x_i|c_j) \] \[\]

Opisane podejście zostało wybrane w niniejszej pracy dyplomowej. 

\subsubsection{Odwrotna częstość wystąpienia (Tf-idf)}
Odwrotna częstość wystąpienia \cite{naiveBayesStanord} to kolejna alternatywa do charakteryzacji dokumentów tekstowych. Może być rozumiana jako ważona częstość wystąpienia, która jest szczególnie użyteczna w przypadku, gdy tekstu nie przefiltrowano pod kątem stop listy. Ta metoda zakłada, że ważność danego słowa jest odwrotnie proporcjonalna do tego, jak często ono występuje we wszystkich dokumentach tekstowych. Pomimo tego, że \blockquote{Tf-idf} jest zwykle używane do klasyfikowania dokumentów poprzez ich podobieństwo (trafność) w różnych zadaniach eksploracji danych (jednym z przykładów jest segregowanie, ustawianie rankingu stron internetowych w wyszukiwarkach), to także metoda odwrotnej częstości wystąpienia może być użyta do klasyfikacji tekstu przy użyciu naiwnego klasyfikatora Bayesa.

\[ Tf-idf = tf_n(t,d)*idf(t) \]

Niech $ tf_n(d,f) $ będzie znormalizowaną częstością wystąpienia, a $ idf $, odwrotną częstością wystąpienia, wtedy obliczenia można wykonać przy pomocy wzoru:

\[ idf(t) = log \frac{n_d}{n_d(t)} \]

gdzie

\begin{itemize}
	\item $n_d$: liczba wszystkich dokumentów,
	\item $n_d(t)$: liczba dokumentów które zawierają wyrażenie t \\
\end{itemize}


\subsection{Skuteczność modelu wielomianowego a modelu Bernoulliego}
Porównania wydajności obu modeli jasno wskazują, że model wielomianowy znacznie przewyższa model Bernoulliego pod względem efektywności, przy założeniu że rozmiar słownika jest odpowiednio wysoki. \cite{multinomialBernoulliModelsEffectiveness}. Jednakże, patrząc z perspektywy algorytmów uczenia maszynowego, to ich skuteczność jest mocno zależna od odpowiedniego doboru cech. W przypadku naiwnego klasyfikatora Bayesa i ogólnie klasyfikacji tekstu duże znaczenie w ostatecznym wyniku ma wybór słów w stop liście, stemming i wybrane tokeny \cite{multinomialBernoulliModelsEffectiveness2}.

\par
W praktyce, przed wyborem danego modelu powinno się porównać różne badania dotyczące tych metod, w tym różne kombinacje etapów wyodrębniania i selekcji cech.

\subsection{Wygładzanie Laplace'a} \label{laplace}
Niezależnie od wybranego modelu w końcu nadejdzie taki moment, kiedy podczas klasyfikacji pojawi się słowo niewystępujące w zbiorze treningowym. W takim przypadku wyliczenie prawdopodobieństwa przynależności do danej klasy decyzyjnej nie ma sensu, gdyż zawsze wynik będzie taki sam - zero. Jest to spowodowane operacją mnożenia występująca we wzorze, a wszystko co zostanie przemnożone przez zero, w wyniku da zero. 

\[ P(c_1|x) = 0*0.99=0 \]
\[ P(c_2|x) = 0*0.11=0 \]

W celu uniknięcia problemu zerowych prawdopodobieństw stosowane jest dodatkowe \textit{wygładzanie}, które polega na dodaniu pewnej wartości do licznika i mianownika. Dla modelu wielomianowego i wygładzania Laplace'a wartość ta to jeden ($ \alpha = 1 $) \cite{naiveBayesStanord}.

\[ P(x_i|c_j) = \frac{N_{x_i, c_j} + \alpha}{N_{c_j} + \alpha*d} \quad (i = (1, ..., d)) \]

gdzie

\begin{itemize}
	\item $N_{x_i, c_j}$: liczba wystąpień cechy $x_i$ w zbiorze treningowym, dla klasy $c_j$
	\item $N_{c_j}$: suma wszystkich cech w klasie $c_j$
	\item $\alpha$: dodatkowy parametr w celu zastosowania wygładzania Laplace'a 
	\item $d$: wymiarowość wektora cechy $x=[x_1, ..., x_d]$ \\
\end{itemize}

\subsection{Ochrona przed przekroczeniem zakresu liczb} \label{overflowPrevention}
Podczas wyliczania prawdopodobieństwa, że dany zbiór cech $x$ należy do klasy $c_j$ mnoży się przez siebie prawdopodobieństwa poszczególnych słów (sekcja nr \ref{termFrequency}). 

\par
Z poziomu implementacji klasyfikatora wygląda to w ten sposób, że używany jest konkretny typ zmiennej, która przechowuje taką wyliczoną wartość (w niniejszej pracy, z powodu użycia języka \textit{C\#} jest to \textit{decimal}). Z racji tego, że mnożone są prawdopodobieństwa mniejsze od zera, to każda taka operacja zbliża wynik do minimalnej wartości tej zmiennej, co może spowodować przekroczenie jej zakresu i w efekcie nieprawidłowy rezultat. 

\par
W celu zabezpieczenia przed powyższym problemem postanowiono skorzystać z właściwości logarytmów \[log(xy) = log(x) + log(y)\] i zamiast mnożyć przez siebie prawdopodobieństwa, to dodać do siebie ich logarytmy \cite{naiveBayesStanord}, co w efekcie da taki sam wynik (taka sama decyzja o przypisanej klasie), a mocno ograniczy możliwość wystąpienia przepełnienia. Poniżej został przedstawiony zmieniony wzór oparty na logarytmach:

\[ c_{NB} = \argmax_{j = 1, ..., n} [logP(c_j) + \sum_{i \in m}logP(x_i|c_j)] \]