﻿using System.IO;

namespace Movies.Data
{
    public static class Dictionary
    {
        public static string ApiUrl = $"http://www.omdbapi.com/?apikey=a80075d0&plot=full&i=";

        public static string Top250IMDBFilePath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\Data\\top250imdb.txt";

        public static string StopWordsFilePath= Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\Data\\stopWords.txt";

        public static string StopWordsTableFilePath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\Data\\stopWordsTable.txt";

        public static string AllCategoriesTop50 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\Data\\allCategoriesTop50.txt";

        public static string FilteredAllCategoriesTop50 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\Data\\filteredAllCategoriesTop50.txt";

        //public static int MovieMaxIndex = 612;

        public static int MovieMaxIndex = 251;
    }
}
