﻿using System.ComponentModel.DataAnnotations;

namespace Movies.Entities
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        public string Title { get; set; }
    }
}
