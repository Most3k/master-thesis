﻿using System.ComponentModel.DataAnnotations;

namespace Movies.Entities
{
    public class Movie
    {
        [Key]
        public int MovieId { get; set; }

        public string ImdbId { get; set; }

        public string Title { get; set; }

        public string Genre { get; set; }

        public string Plot { get; set; }
    }
}