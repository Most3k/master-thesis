﻿using System.ComponentModel.DataAnnotations;

namespace Movies.Entities
{
    public class MovieDuplicated
    {
        [Key]
        public int MovieId { get; set; }

        public int MovieParentId { get; set; }

        public string ImdbId { get; set; }

        public string Title { get; set; }

        public string Genre { get; set; }

        public string Plot { get; set; }
    }
}