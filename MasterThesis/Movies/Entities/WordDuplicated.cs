﻿using System.ComponentModel.DataAnnotations;

namespace Movies.Entities
{
    public class WordDuplicated
    {
        [Key]
        public int WordId { get; set; }

        public int MovieId { get; set; }

        public int MovieParentId { get; set; }

        public string Text { get; set; }

        public int Occurred { get; set; }
    }
}
