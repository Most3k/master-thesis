﻿using System.Collections.Generic;
using Movies.Entities;

namespace Movies.Interfaces
{
    public interface ICategoriesRepository : IRepository<Category>
    {
        IEnumerable<Category> GetAll();

        void AddMany(IEnumerable<string> categories);
    }
}
