﻿using System.Collections.Generic;
using Movies.Entities;

namespace Movies.Interfaces
{
    public interface IMoviesRepository : IRepository<Movie>
    {
        IEnumerable<Movie> GetAll();

        bool IsMovieAdded(string imdbId);
    }
}
