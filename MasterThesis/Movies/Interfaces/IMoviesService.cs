﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Movies.Entities;

namespace Movies.Interfaces
{
    public interface IMoviesService
    {
        Task FillTheDatabase(IEnumerable<string> movieImdbIds);

        ICollection<Movie> GetMovies(int moviesFromDBCount);
    }
}
