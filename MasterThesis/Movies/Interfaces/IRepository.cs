﻿namespace Movies.Interfaces
{
    public interface IRepository<T>
    {
        T Add(T item);

        T Get(int id);

        T Update(T item);

        void Remove(int id);
    }
}
