﻿using System.Collections.Generic;
using Movies.Entities;

namespace Movies.Interfaces
{
    public interface IWordsRepository : IRepository<Word>
    {
        void AddMany(IEnumerable<Word> words);

        IEnumerable<Word> GetByMovieId(int movieId);

        ICollection<Word> GetAll();
    }
}
