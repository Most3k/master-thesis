﻿using System.Collections.Generic;
using Movies.Entities;

namespace Movies.Interfaces
{
    public interface IWordsService
    {
        void FillTheDatabase(ICollection<string> stopWords);

        IEnumerable<Word> GetWordsByMovie(IEnumerable<Movie> movies);
    }
}