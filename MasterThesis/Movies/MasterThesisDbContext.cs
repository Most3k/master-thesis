﻿using System.Data.Entity;
using Movies.Entities;

namespace Movies
{
    public class MasterThesisDbContext : DbContext
    {
        public MasterThesisDbContext() : base("MoviesDbContext") { }

        public DbSet<Movie> Movies { get; set; }

        public DbSet<Word> Words { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<MovieDuplicated> MoviesDuplicated { get; set; }

        public DbSet<WordDuplicated> WordsDuplicated { get; set; }
    }
}
