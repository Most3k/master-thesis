namespace Movies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Occurred_Column_To_Words_Table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Words", "Occurred", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Words", "Occurred");
        }
    }
}
