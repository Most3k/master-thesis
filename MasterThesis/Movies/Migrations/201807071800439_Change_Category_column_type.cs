namespace Movies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Category_column_type : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "Title", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Categories", "Title", c => c.Int(nullable: false));
        }
    }
}
