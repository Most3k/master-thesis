namespace Movies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Movies_And_Words_Duplicated_Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MovieDuplicateds",
                c => new
                    {
                        MovieId = c.Int(nullable: false, identity: true),
                        ImdbId = c.String(),
                        Title = c.String(),
                        Genre = c.String(),
                        Plot = c.String(),
                    })
                .PrimaryKey(t => t.MovieId);
            
            CreateTable(
                "dbo.WordDuplicateds",
                c => new
                    {
                        WordId = c.Int(nullable: false, identity: true),
                        MovieId = c.Int(nullable: false),
                        Text = c.String(),
                        Occurred = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.WordId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.WordDuplicateds");
            DropTable("dbo.MovieDuplicateds");
        }
    }
}
