namespace Movies.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_MovieParentId_Column_To_Duplicated_Tables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MovieDuplicateds", "MovieParentId", c => c.Int(nullable: false));
            AddColumn("dbo.WordDuplicateds", "MovieParentId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WordDuplicateds", "MovieParentId");
            DropColumn("dbo.MovieDuplicateds", "MovieParentId");
        }
    }
}
