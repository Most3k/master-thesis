﻿using System;
using System.Collections.Generic;
using System.IO;
using Movies.Repositories;
using Movies.Services;
using Movies.Data;
using System.Linq;
using Movies.Entities;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace Movies
{
    class Program
    {
        static void Main(string[] args)
        {
            //string top250path = Dictionary.Top250IMDBFilePath;
            //string allCategoriesTop50 = Dictionary.AllCategoriesTop50;
            //string filteredAllCategoriesTop50 = Dictionary.FilteredAllCategoriesTop50;

            var filesService = new FilesService();
            //filesService.PrepareDataToTableFromFile(Dictionary.StopWordsFilePath, 6, Dictionary.StopWordsTableFilePath);
            //filesService.FilterFileByExistingMoviesInDB(allCategoriesTop50, filteredAllCategoriesTop50);

            //IEnumerable<string> imdbIds = filesService.ReadFile(top250path);
            //IEnumerable<string> imdbIds = filesService.ReadFile(filteredAllCategoriesTop50);
            //var moviesService = new MoviesService();
            //moviesService.FillTheDatabase(imdbIds);

            //string stopWordsPath = Dictionary.StopWordsFilePath;
            //IEnumerable<string> stopWords = filesService.ReadFile(stopWordsPath);

            //var wordsService = new WordsService();
            //wordsService.FillTheDatabase(stopWords.ToList());

            //var categoriesService = new CategoriesService();
            //categoriesService.FillTheDatabase();

            //int count = 0;

            //using (var dbContext = new MasterThesisDbContext())
            //{
            //    var groupedWords = dbContext.Words.GroupBy(w => w.MovieId).ToList();

            //    foreach (var item in groupedWords)
            //    {
            //        if (item.Count() > 40)
            //        {
            //            count++;
            //        }

            //        Console.WriteLine(item.Count());
            //    }
            //}

            //var moviesService = new MoviesDuplicatedService();
            //moviesService.FillTheDatabase();


            string stopWordsPath = Dictionary.StopWordsFilePath;
            IEnumerable<string> stopWords = filesService.ReadFile(stopWordsPath);
            var wordsService = new WordsDuplicatedService();
            wordsService.FillTheDatabase(stopWords.ToList());
        }
    }
}
