﻿using System.Collections.Generic;
using System.Linq;
using Movies.Entities;
using Movies.Interfaces;

namespace Movies.Repositories
{
    public class CategoriesRepository : ICategoriesRepository
    {
        public Category Add(Category item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                Category category = dbContext.Categories.Add(item);

                dbContext.SaveChanges();

                return category;
            }
        }

        public void AddMany(IEnumerable<string> categories)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                foreach (string category in categories)
                {
                    dbContext.Categories.Add(new Category()
                    {
                        Title = category
                    });
                }

                dbContext.SaveChanges();
            }
        }

        public Category Get(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.Categories.Where(item => item.CategoryId == id).FirstOrDefault();
            }
        }

        public IEnumerable<Category> GetAll()
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.Categories.ToList();
            }
        }

        public void Remove(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                Category category = dbContext.Categories.Where(item => item.CategoryId == id).FirstOrDefault();

                dbContext.Categories.Remove(category);

                dbContext.SaveChanges();
            }
        }

        public Category Update(Category item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                Category category = dbContext.Categories.Where(m => m.CategoryId == item.CategoryId).FirstOrDefault();


                dbContext.SaveChanges();

                return category;
            }
        }
    }
}
