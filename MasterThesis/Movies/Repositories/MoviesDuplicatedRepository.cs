﻿using System.Collections.Generic;
using System.Linq;
using Movies.Entities;

namespace Movies.Repositories
{
    public class MoviesDuplicatedRepository
    {
        public MovieDuplicated Add(MovieDuplicated item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                MovieDuplicated movie = dbContext.MoviesDuplicated.Add(item);

                dbContext.SaveChanges();

                return movie;
            }
        }

        public void AddMany(IEnumerable<MovieDuplicated> moviesDuplicated)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                foreach (MovieDuplicated md in moviesDuplicated)
                {
                    dbContext.MoviesDuplicated.Add(md);
                }

                dbContext.SaveChanges();
            }
        }

        public MovieDuplicated Get(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.MoviesDuplicated.Where(item => item.MovieId == id).FirstOrDefault();
            }
        }

        public IEnumerable<MovieDuplicated> GetAll()
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.MoviesDuplicated.ToList();
            }
        }

        public bool IsMovieAdded(string imdbId)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.MoviesDuplicated.Any(movie => movie.ImdbId == imdbId);
            }
        }

        public void Remove(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                MovieDuplicated movie = dbContext.MoviesDuplicated.Where(item => item.MovieId == id).FirstOrDefault();

                dbContext.MoviesDuplicated.Remove(movie);

                dbContext.SaveChanges();
            }
        }

        public MovieDuplicated Update(MovieDuplicated item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                MovieDuplicated movie = dbContext.MoviesDuplicated.Where(m => m.MovieId == item.MovieId).FirstOrDefault();

                movie.Genre = item.Genre;
                movie.ImdbId = item.ImdbId;
                movie.Plot = item.Plot;
                movie.Title = item.Title;

                dbContext.SaveChanges();

                return movie;
            }
        }
    }
}
