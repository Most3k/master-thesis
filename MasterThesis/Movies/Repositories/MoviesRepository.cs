﻿using System.Collections.Generic;
using System.Linq;
using Movies.Entities;
using Movies.Interfaces;

namespace Movies.Repositories
{
    public class MoviesRepository : IMoviesRepository
    {
        public Movie Add(Movie item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                Movie movie = dbContext.Movies.Add(item);

                dbContext.SaveChanges();

                return movie;
            }
        }

        public Movie Get(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.Movies.Where(item => item.MovieId == id).FirstOrDefault();
            }
        }

        public IEnumerable<Movie> GetAll()
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.Movies.ToList();
            }
        }

        public bool IsMovieAdded(string imdbId)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.Movies.Any(movie => movie.ImdbId == imdbId);
            }
        }

        public void Remove(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                Movie movie = dbContext.Movies.Where(item => item.MovieId == id).FirstOrDefault();

                dbContext.Movies.Remove(movie);

                dbContext.SaveChanges();
            }
        }

        public Movie Update(Movie item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                Movie movie = dbContext.Movies.Where(m => m.MovieId == item.MovieId).FirstOrDefault();

                movie.Genre = item.Genre;
                movie.ImdbId = item.ImdbId;
                movie.Plot = item.Plot;
                movie.Title = item.Title;
                
                dbContext.SaveChanges();

                return movie;
            }
        }
    }
}
