﻿using System.Collections.Generic;
using System.Linq;
using Movies;
using Movies.Entities;

namespace WordsDuplicated.Repositories
{
    public class WordsDuplicatedRepository
    {
        public WordDuplicated Add(WordDuplicated item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                WordDuplicated word = dbContext.WordsDuplicated.Add(item);

                dbContext.SaveChanges();

                return word;
            }
        }

        public void AddMany(IEnumerable<WordDuplicated> words)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                foreach (WordDuplicated word in words)
                {
                    dbContext.WordsDuplicated.Add(word);
                }

                dbContext.SaveChanges();
            }
        }

        public WordDuplicated Get(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.WordsDuplicated.Where(item => item.WordId == id).FirstOrDefault();
            }
        }

        public IEnumerable<WordDuplicated> GetByMovieId(int movieId)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.WordsDuplicated.Where(item => item.MovieId == movieId).ToList();
            }
        }

        public ICollection<WordDuplicated> GetAll()
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.WordsDuplicated.ToList();
            }
        }

        public void Remove(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                WordDuplicated word = dbContext.WordsDuplicated.Where(item => item.WordId == id).FirstOrDefault();

                dbContext.WordsDuplicated.Remove(word);

                dbContext.SaveChanges();
            }
        }

        public WordDuplicated Update(WordDuplicated item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                WordDuplicated word = dbContext.WordsDuplicated.Where(m => m.WordId == item.WordId).FirstOrDefault();

                word.MovieId = item.MovieId;
                word.Text = item.Text;

                dbContext.SaveChanges();

                return word;
            }
        }
    }
}
