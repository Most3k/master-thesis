﻿using System;
using System.Collections.Generic;
using System.Linq;
using Movies;
using Movies.Entities;
using Movies.Interfaces;

namespace Words.Repositories
{
    public class WordsRepository : IWordsRepository
    {
        public Word Add(Word item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                Word word = dbContext.Words.Add(item);

                dbContext.SaveChanges();

                return word;
            }
        }

        public void AddMany(IEnumerable<Word> words)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                foreach (Word word in words)
                {
                    dbContext.Words.Add(word);
                }

                dbContext.SaveChanges();
            }
        }

        public Word Get(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.Words.Where(item => item.WordId == id).FirstOrDefault();
            }
        }

        public IEnumerable<Word> GetByMovieId(int movieId)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.Words.Where(item => item.MovieId == movieId).ToList();
            }
        }

        public ICollection<Word> GetAll()
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                return dbContext.Words.ToList();
            }
        }

        public void Remove(int id)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                Word word = dbContext.Words.Where(item => item.WordId == id).FirstOrDefault();

                dbContext.Words.Remove(word);

                dbContext.SaveChanges();
            }
        }

        public Word Update(Word item)
        {
            using (var dbContext = new MasterThesisDbContext())
            {
                Word word = dbContext.Words.Where(m => m.WordId == item.WordId).FirstOrDefault();

                word.MovieId = item.MovieId;
                word.Text = item.Text;

                dbContext.SaveChanges();

                return word;
            }
        }
    }
}
