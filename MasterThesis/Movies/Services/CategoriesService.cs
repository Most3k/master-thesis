﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using Movies.Entities;
using Movies.Interfaces;
using Movies.Repositories;

namespace Movies.Services
{
    public class CategoriesService
    {
        private IMoviesRepository _moviesRepository;
        private ICategoriesRepository _categoriesRepository;

        public CategoriesService()
        {
            _moviesRepository = new MoviesRepository();
            _categoriesRepository = new CategoriesRepository();
        }

        public CategoriesService(IMoviesRepository moviesRepository, ICategoriesRepository categoriesRepository)
        {
            _moviesRepository = moviesRepository;
            _categoriesRepository = categoriesRepository;
        }

        public void FillTheDatabase()
        {
            var uniqueCategories = new Collection<string>();
            IEnumerable<Movie> uniqueCategoriesMovies = _moviesRepository.GetAll()
                .GroupBy(movie => movie.Genre)
                .Select(group => group.First())
                .ToList();

            foreach (Movie movie in uniqueCategoriesMovies)
            {
                string[] splittedCategories = movie.Genre.Split(',');

                foreach (string category in splittedCategories)
                {
                    string text = Regex.Replace(category, @"\s+", "");

                    if (!uniqueCategories.Contains(text))
                    {
                        uniqueCategories.Add(text);
                    }
                }
            }

            _categoriesRepository.AddMany(uniqueCategories);
        }
    }
}
