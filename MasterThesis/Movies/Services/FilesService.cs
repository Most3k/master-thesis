﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Movies.Repositories;

namespace Movies.Services
{
    public class FilesService
    {
        public IEnumerable<string> ReadFile(string path)
        {
            string[] lines = File.ReadAllLines(path);

            return new Collection<string>(lines);
        }

        public void FilterFileByExistingMoviesInDB(string sourceFilePath, string destinationFilePath)
        {
            IEnumerable<string> imdbMoviesIds = new MoviesRepository().GetAll().Select(movie => movie.ImdbId);
            var imdbMoviesIdsToAddToFile = new Collection<string>();

            using (StreamReader reader = File.OpenText(sourceFilePath))
            {
                string imdbId = string.Empty;

                while ((imdbId = reader.ReadLine()) != null)
                {
                    if (imdbMoviesIds.Contains(imdbId) == false && imdbMoviesIdsToAddToFile.Contains(imdbId) == false)
                    {
                        imdbMoviesIdsToAddToFile.Add(imdbId);
                    }
                }
            }

            using (StreamWriter writer = File.AppendText(destinationFilePath))
            {
                foreach (string imdbIdToAdd in imdbMoviesIdsToAddToFile)
                {
                    writer.WriteLine(imdbIdToAdd);
                }
            }
        }

        public void PrepareDataToTableFromFile(string sourceFilePath, int newLineAfterNumberOfColumns, string destinationFilePath)
        {
            using (StreamReader reader = File.OpenText(sourceFilePath))
            using (StreamWriter writer = File.AppendText(destinationFilePath))
            {
                string line = string.Empty;
                string lineToSave = string.Empty;
                int newLineCounter = 0;

                while ((line = reader.ReadLine()) != null)
                {
                    lineToSave += line + ',';
                    newLineCounter++;

                    if (newLineCounter == newLineAfterNumberOfColumns)
                    {
                        writer.WriteLine(lineToSave);
                        newLineCounter = 0;
                        lineToSave = string.Empty;
                    }
                }
            }
        }
    }
}
