﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Movies.Data;
using Movies.Entities;
using Movies.Repositories;
using Words.Repositories;
using WordsDuplicated.Repositories;

namespace Movies.Services
{
    public class MoviesDuplicatedService
    {
        private MoviesDuplicatedRepository _moviesDuplicatedRepository;

        public MoviesDuplicatedService()
        {
            _moviesDuplicatedRepository = new MoviesDuplicatedRepository();
        }

        public MoviesDuplicatedService(MoviesDuplicatedRepository moviesRepository)
        {
            _moviesDuplicatedRepository = moviesRepository;
        }

        public void FillTheDatabase()
        {
            IEnumerable<MovieDuplicated> moviesDuplicated = _moviesDuplicatedRepository.GetAll();
            var moviesToAdd = new Collection<MovieDuplicated>();

            foreach (Movie movie in new MoviesRepository().GetAll().Where(m => m.MovieId < Dictionary.MovieMaxIndex))
            {
                if (moviesDuplicated.Any(m => m.ImdbId == movie.ImdbId))
                {
                    continue;
                }

                string[] splittedCategories = movie.Genre.Split(',');

                foreach (string cat in splittedCategories)
                {
                    string defaultCatCleaned = CleanString(cat);

                    var md = new MovieDuplicated()
                    {
                        MovieParentId = movie.MovieId,
                        ImdbId = movie.ImdbId,
                        Genre = defaultCatCleaned,
                        Plot = movie.Plot,
                        Title = movie.Title
                    };

                    moviesToAdd.Add(md);                    
                }
            }

            _moviesDuplicatedRepository.AddMany(moviesToAdd);
        }

        public ICollection<MovieDuplicated> GetMovies(int moviesFromDBCount)
        {
            var groupedMovies = new MoviesDuplicatedRepository()
                .GetAll()
                .GroupBy(word => word.Genre)
                .OrderByDescending(group => group.Count())
                .ToList();

            ICollection<MovieDuplicated> movies = groupedMovies.Where(m => m.Count() > 20).SelectMany(d => d).ToList();

            return movies;

            //return _moviesDuplicatedRepository.GetAll().Join(movieIds, word => word.Genre, movieId => movieId, (word, movieId) => word).ToList();


            //    var movieIds = new WordsDuplicatedRepository()
            //        .GetAll()
            //        .GroupBy(word => word.MovieId)
            //        .OrderByDescending(group => group.Count())
            //        .Take(moviesFromDBCount)
            //        .Select(id => id.Key)
            //        .ToList();

            //    return _moviesDuplicatedRepository.GetAll().Join(movieIds, word => word.MovieId, movieId => movieId, (word, movieId) => word).ToList();

            //return _moviesRepository
            //   .GetAll()
            //   .OrderBy(movie => Guid.NewGuid())
            //   .Take(moviesFromDBCount)
            //   .ToList();
        }

        private static string CleanString(string dirty)
        {
            string cleaned = new string(dirty.Where(c => !char.IsPunctuation(c)).ToArray()).ToLower();

            return Regex.Replace(cleaned, @"\s+", "");
        }
    }
}
