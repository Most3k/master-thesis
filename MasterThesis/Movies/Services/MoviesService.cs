﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Movies.Data;
using Movies.Entities;
using Movies.Interfaces;
using Movies.Repositories;
using Newtonsoft.Json.Linq;
using Words.Repositories;

namespace Movies.Services
{
    public class MoviesService : IMoviesService
    {
        private IMoviesRepository _moviesRepository;

        public MoviesService()
        {
            _moviesRepository = new MoviesRepository();
        }

        public MoviesService(IMoviesRepository moviesRepository)
        {
            _moviesRepository = moviesRepository;
        }

        public async Task FillTheDatabase(IEnumerable<string> movieImdbIds)
        {
            IEnumerable<Movie> movies = _moviesRepository.GetAll();

            foreach (string movieImdbId in movieImdbIds)
            {
                if (movies.Any(movie => movie.ImdbId == movieImdbId))
                {
                    continue;
                }

                _moviesRepository.Add(await GetMovieData(movieImdbId));
            }
        }

        private async Task<Movie> GetMovieData(string movieId)
        {
            string url = Dictionary.ApiUrl + movieId;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            Thread.Sleep(400);

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                JObject jObject = JObject.Parse(reader.ReadToEnd());

                return new Movie()
                {
                    ImdbId = jObject["imdbID"].ToString(),
                    Genre = jObject["Genre"].ToString(),
                    Plot = jObject["Plot"].ToString(),
                    Title = jObject["Title"].ToString()
                };
            }
        }

        public ICollection<Movie> GetMovies(int moviesFromDBCount)
        {
            var groupedMovies = new MoviesRepository()
                .GetAll()
                .Where(m => m.MovieId < Dictionary.MovieMaxIndex)
                .GroupBy(word => word.Genre)
                .OrderByDescending(group => group.Count())
                .ToList();

            ICollection<Movie> movies = groupedMovies.Where(m => m.Count() > 5).SelectMany(d => d).ToList();

            return movies;

            //var movieIds = new WordsRepository()
            //    .GetAll()
            //    .Where(m => m.MovieId < Dictionary.MovieMaxIndex)
            //    .GroupBy(word => word.MovieId)
            //    .OrderByDescending(group => group.Count())
            //    .Take(moviesFromDBCount)
            //    .Select(id => id.Key)
            //    .ToList();

            //return _moviesRepository.GetAll().Join(movieIds, word => word.MovieId, movieId => movieId, (word, movieId) => word).ToList();

            //return _moviesRepository
            //   .GetAll()
            //   .OrderBy(movie => Guid.NewGuid())
            //   .Take(moviesFromDBCount)
            //   .ToList();
        }
    }
}
