﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Movies.Entities;
using Movies.Repositories;
using Words.Repositories;
using WordsDuplicated.Repositories;

namespace Movies.Services
{
    public class WordsDuplicatedService
    {
        private ICollection<string> _stopWords;

        public void FillTheDatabase(ICollection<string> stopWords)
        {
            _stopWords = stopWords;
            var moviesDuplicatedRepository = new MoviesDuplicatedRepository();
            var wordsDuplicatedRepository = new WordsDuplicatedRepository();
            var movieDuplicatedService = new MoviesDuplicatedService(moviesDuplicatedRepository);

            int count = moviesDuplicatedRepository.GetAll().Count();

            foreach (MovieDuplicated movie in moviesDuplicatedRepository.GetAll())
            {
                if (wordsDuplicatedRepository.GetByMovieId(movie.MovieId).Count() > 0)
                {
                    continue;
                }

                IEnumerable<string> sievedWords = SieveWordsUsingStopWords(movie.Plot);
                IEnumerable<WordDuplicated> sievedWordsWithoutDuplicates = CountDuplicatedWords(movie.MovieId, sievedWords);

                wordsDuplicatedRepository.AddMany(sievedWordsWithoutDuplicates);
            }
        }

        public IEnumerable<WordDuplicated> GetWordsByMovie(IEnumerable<MovieDuplicated> movies)
        {
            IEnumerable<int> movieIds = movies.Select(movie => movie.MovieId);

            return new WordsDuplicatedRepository().GetAll().Join(movieIds, word => word.MovieId, movieId => movieId, (word, movieId) => word);
        }

        private IEnumerable<string> SieveWordsUsingStopWords(string plot)
        {
            ICollection<string> sievedWords = new Collection<string>();
            string[] splitted = plot.Split(' ');

            foreach (string word in splitted)
            {
                string wordToLowerWithoutPunctuationChars = new string(word
                    .Where(c => !char.IsPunctuation(c)).ToArray()).ToLower();

                if (!_stopWords.Contains(wordToLowerWithoutPunctuationChars))
                {
                    sievedWords.Add(wordToLowerWithoutPunctuationChars);
                }
            }

            return sievedWords;
        }

        private IEnumerable<WordDuplicated> CountDuplicatedWords(int movieId, IEnumerable<string> sievedWords)
        {
            ICollection<WordDuplicated> words = new Collection<WordDuplicated>();

            foreach (string wordText in sievedWords)
            {
                if (!words.Any(x => x.Text == wordText))
                {
                    words.Add(new WordDuplicated()
                    {
                        MovieId = movieId,
                        Text = wordText,
                        Occurred = sievedWords.Where(x => x == wordText).Count()
                    });
                }
            }

            return words;
        }
    }
}
