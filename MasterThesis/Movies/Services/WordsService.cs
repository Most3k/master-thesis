﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Movies.Entities;
using Movies.Interfaces;
using Movies.Repositories;
using Words.Repositories;

namespace Movies.Services
{
    public class WordsService : IWordsService
    {
        private ICollection<string> _stopWords;

        public void FillTheDatabase(ICollection<string> stopWords)
        {
            _stopWords = stopWords;
            var moviesRepository = new MoviesRepository();
            var wordsRepository = new WordsRepository();
            var movieService = new MoviesService(moviesRepository);

            foreach (Movie movie in moviesRepository.GetAll())
            {
                if (wordsRepository.GetByMovieId(movie.MovieId).Count() > 0)
                {
                    continue;
                }

                IEnumerable<string> sievedWords = SieveWordsUsingStopWords(movie.Plot);
                IEnumerable<Word> sievedWordsWithoutDuplicates = CountDuplicatedWords(movie.MovieId, sievedWords);

                wordsRepository.AddMany(sievedWordsWithoutDuplicates);
            }
        }

        public IEnumerable<Word> GetWordsByMovie(IEnumerable<Movie> movies)
        {
            IEnumerable<int> movieIds = movies.Select(movie => movie.MovieId);

            return new WordsRepository().GetAll().Join(movieIds, word => word.MovieId, movieId => movieId, (word, movieId) => word);
        }


        private IEnumerable<string> SieveWordsUsingStopWords(string plot)
        {
            ICollection<string> sievedWords = new Collection<string>();
            string[] splitted = plot.Split(' ');

            foreach (string word in splitted)
            {
                string wordToLowerWithoutPunctuationChars = new string(word
                    .Where(c => !char.IsPunctuation(c)).ToArray()).ToLower();

                if (!_stopWords.Contains(wordToLowerWithoutPunctuationChars))
                {
                    sievedWords.Add(wordToLowerWithoutPunctuationChars);
                }
            }

            return sievedWords;
        }

        private IEnumerable<Word> CountDuplicatedWords(int movieId, IEnumerable<string> sievedWords)
        {
            ICollection<Word> words = new Collection<Word>();

            foreach (string wordText in sievedWords)
            {
                if (!words.Any(x => x.Text == wordText))
                {
                    words.Add(new Word()
                    {
                        MovieId = movieId,
                        Text = wordText,
                        Occurred = sievedWords.Where(x => x == wordText).Count()
                    });
                }
            }

            return words;
        }
    }
}
