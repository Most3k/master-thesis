﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Movies.Entities;
using NaiveBayesClassifier.Model;

namespace NaiveBayesClassifier
{
    public class Classifier
    {
        private int _testSetIndex = 0;
        private int _denominator;
        private IList<Movie> _movies;
        private ICollection<Word> _moviesWords;
        private IList<ICollection<Movie>> _movieSets;
        private IList<Prior> _priors;
        private ICollection<Likelihood> _likelihoods;
        private ICollection<string> _categories;
        private ICollection<Movie> _trainingMovies;

        public Classifier(IList<Movie> movies, ICollection<Word> movieWords, IList<ICollection<Movie>> movieSets)
        {
            _movies = movies;
            _moviesWords = movieWords;
            _movieSets = movieSets;
        }

        public ICollection<KeyValuePair<string, ClassifierSetEffectiveness>> Classificate()
        {
            var setsEffectivenesses = new Collection<IDictionary<string, ClassifierSetEffectiveness>>();
            double overallEffectiveness = 0;

            for (; _testSetIndex < Settings.SetsCount; _testSetIndex++)
            {
                PrepareTrainingSet();
                CalculatePriors();
                CalculateLikelihoods();

                IDictionary<Movie, string> classifications = ChooseCategoriesForMovies();
                IDictionary<string, ConfusionMatrix> confusionMatrix = Effectiveness.CalculateConfusionMatrix(classifications);
                IDictionary<string, ClassifierSetEffectiveness> classifierEffectiveness = Effectiveness.GetSetEffectiveness(confusionMatrix);
                overallEffectiveness += Effectiveness.CheckOverallClassifierCorrectness(classifications);

                setsEffectivenesses.Add(classifierEffectiveness);

                //Tuple<double, double> optymisticCalculations = Effectiveness.CheckClassifierCorrectnessInOptymisticWay(classifications);                
            }

            overallEffectiveness /= Settings.SetsCount;

            return Effectiveness.CalculateClassifierEffectiveness(setsEffectivenesses);
        }

        private void PrepareTrainingSet()
        {
            _priors = new Collection<Prior>();
            _categories = new Collection<string>();
            _trainingMovies = new Collection<Movie>();

            IEnumerable<int> testMoviesIds = _movieSets[_testSetIndex].Select(movie => movie.MovieId);

            foreach (Movie movie in _movies)
            {
                // if current movie is not in test set
                if (testMoviesIds.Contains(movie.MovieId) == false)
                {
                    _trainingMovies.Add(movie);
                }
            }
        }

        private void CalculatePriors()
        {
            IEnumerable<IGrouping<string, Movie>> categorized = _trainingMovies.GroupBy(movie => movie.Genre);

            foreach (IGrouping<string, Movie> categoryGroup in categorized)
            {
                double val = (double)categoryGroup.Count() / Settings.TrainingSetMoviesCount;

                _priors.Add(new Prior()
                {
                    Category = categoryGroup.Key,
                    Value = val
                });

                _categories.Add(categoryGroup.Key);
            }
        }

        private void CalculateLikelihoods()
        {
            bool shouldCalculateDenominator;
            _likelihoods = new Collection<Likelihood>();

            foreach (string category in _categories)
            {
                shouldCalculateDenominator = true;

                foreach (Movie movie in _movieSets[_testSetIndex])
                {
                    IEnumerable<Word> currentMovieWords = _moviesWords.Where(word => word.MovieId == movie.MovieId);

                    foreach (Word word in currentMovieWords)
                    {
                        _likelihoods.Add(CalculateLikelihood(ref shouldCalculateDenominator, category, movie.MovieId, word));
                    }
                }
            }
        }

        private IDictionary<Movie, string> ChooseCategoriesForMovies()
        {
            IDictionary<Movie, string> classifications = new Dictionary<Movie, string>();

            foreach (Movie movie in _movieSets[_testSetIndex])
            {
                var categoriesLikelihoods = new Dictionary<string, double>();

                foreach (string category in _categories)
                {
                    double result = Math.Log10(_priors.FirstOrDefault(p => p.Category == category).Value);

                    IEnumerable<Likelihood> likelihoods = _likelihoods.Where(likelihood => likelihood.Category == category && likelihood.MovieId == movie.MovieId);

                    foreach (Likelihood likelihood in likelihoods)
                    {
                        double logarithm = Math.Log10(likelihood.Value);

                        result += likelihood.Occurred > 1 ? logarithm * likelihood.Occurred : logarithm;
                    }

                    categoriesLikelihoods.Add(category, result);
                }

                string outputCategory = categoriesLikelihoods.OrderByDescending(cat => cat.Value).Select(cat => cat.Key).First();

                classifications.Add(movie, outputCategory);
            }

            return classifications;
        }

        private Likelihood CalculateLikelihood(ref bool shouldCalculateDenominator, string category, int movieId, Word word)
        {
            if (shouldCalculateDenominator)
            {
                _denominator = GetAllWordsCountInTrainingSetForCategory(category) + GetUniqueWordsCountInTrainingSet();
                shouldCalculateDenominator = false;
            }

            return new Likelihood()
            {
                Category = category,
                MovieId = movieId,
                Value = (double)(GetWordOccuranceCountInTrainingSetForCategory(category, word.Text) + 1) / (_denominator),
                Occurred = word.Occurred,
                WordId = word.WordId
            };
        }

        private int GetAllWordsCountInTrainingSetForCategory(string category)
        {
            int allWordsCount = 0;

            for (int i = 0; i < _movieSets.Count; i++)
            {
                if (i == _testSetIndex)
                {
                    continue;
                }

                foreach (Movie movie in _movieSets[i])
                {
                    if (movie.Genre != category)
                    {
                        continue;
                    }

                    IEnumerable<Word> words = _moviesWords.Where(w => w.MovieId == movie.MovieId);

                    foreach (Word word in words)
                    {
                        allWordsCount += word.Occurred;
                    }
                }
            }

            return allWordsCount;
        }

        private int GetWordOccuranceCountInTrainingSetForCategory(string category, string wordText)
        {
            int wordOccuranceCount = 0;

            for (int i = 0; i < _movieSets.Count; i++)
            {
                if (i == _testSetIndex)
                {
                    continue;
                }

                foreach (Movie movie in _movieSets[i].Where(m => m.Genre == category))
                {
                    Word word = _moviesWords.FirstOrDefault(w => w.MovieId == movie.MovieId && w.Text == wordText);

                    wordOccuranceCount += word != null ? word.Occurred : 0;
                }
            }

            return wordOccuranceCount;
        }

        private int GetUniqueWordsCountInTrainingSet()
        {
            var words = new Collection<Word>();

            for (int i = 0; i < _movieSets.Count; i++)
            {
                if (i == _testSetIndex)
                {
                    continue;
                }

                foreach (Movie movie in _movieSets[i])
                {
                    IEnumerable<Word> movieWords = _moviesWords.Where(w => w.MovieId == movie.MovieId);

                    foreach (Word word in movieWords)
                    {
                        if (words.Any(w => w.Text == word.Text) == false)
                        {
                            words.Add(word);
                        }
                    }
                }
            }

            return words.Count();
        }
    }
}