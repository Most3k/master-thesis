﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using Movies.Data;
using Movies.Entities;
using Movies.Repositories;
using NaiveBayesClassifier.Model;

namespace NaiveBayesClassifier
{
    public class ClassifierDuplicated
    {
        private int _testSetIndex = 0;
        private int _denominator;
        private IList<MovieDuplicated> _movies;
        private ICollection<WordDuplicated> _moviesWords;
        private IList<ICollection<MovieDuplicated>> _movieSets;
        private IList<Prior> _priors;
        private ICollection<Likelihood> _likelihoods;
        private ICollection<string> _categories;
        private ICollection<MovieDuplicated> _trainingMovies;
        private IEnumerable<Movie> _originalMovies;

        public ClassifierDuplicated(IList<MovieDuplicated> movies, ICollection<WordDuplicated> movieWords, IList<ICollection<MovieDuplicated>> movieSets)
        {
            _movies = movies;
            _moviesWords = movieWords;
            _movieSets = movieSets;
            _originalMovies = new MoviesRepository().GetAll().Where(m => m.MovieId < Dictionary.MovieMaxIndex).ToList();
        }

        public ICollection<KeyValuePair<string, ClassifierSetEffectiveness>> Classificate()
        {
            var setsEffectivenesses = new Collection<IDictionary<string, ClassifierSetEffectiveness>>();
            double overallEffectiveness = 0;

            for (; _testSetIndex < Settings.SetsCount; _testSetIndex++)
            {
                PrepareTrainingSet();
                CalculatePriors();
                CalculateLikelihoods();

                IDictionary<MovieDuplicated, List<string>> classifications = ChooseCategoriesForMovies();
                IDictionary<string, ConfusionMatrix> confusionMatrix = EffectivenessDuplicated.CalculateConfusionMatrix(classifications);
                IDictionary<string, ClassifierSetEffectiveness> classifierEffectiveness = EffectivenessDuplicated.GetSetEffectiveness(confusionMatrix);
                overallEffectiveness += EffectivenessDuplicated.CheckOverallClassifierCorrectness(classifications);

                setsEffectivenesses.Add(classifierEffectiveness);

                //Tuple<double, double> optymisticCalculations = Effectiveness.CheckClassifierCorrectnessInOptymisticWay(classifications);                
            }

            overallEffectiveness /= Settings.SetsCount;

            return Effectiveness.CalculateClassifierEffectiveness(setsEffectivenesses);
        }

        private void PrepareTrainingSet()
        {
            _priors = new Collection<Prior>();
            _categories = new Collection<string>();
            _trainingMovies = new Collection<MovieDuplicated>();

            IEnumerable<int> testMoviesIds = _movieSets[_testSetIndex].Select(movie => movie.MovieId);

            foreach (MovieDuplicated movie in _movies)
            {
                // if current movie is not in test set
                if (testMoviesIds.Contains(movie.MovieId) == false)
                {
                    _trainingMovies.Add(movie);
                }
            }
        }

        private void CalculatePriors()
        {
            IEnumerable<IGrouping<string, MovieDuplicated>> categorized = _trainingMovies.GroupBy(movie => movie.Genre);

            foreach (IGrouping<string, MovieDuplicated> categoryGroup in categorized)
            {
                double val = (double)categoryGroup.Count() / Settings.TrainingSetMoviesCount;

                _priors.Add(new Prior()
                {
                    Category = categoryGroup.Key,
                    Value = val
                });

                _categories.Add(categoryGroup.Key);
            }
        }

        private void CalculateLikelihoods()
        {
            bool shouldCalculateDenominator;
            _likelihoods = new Collection<Likelihood>();

            foreach (string category in _categories)
            {
                shouldCalculateDenominator = true;

                foreach (MovieDuplicated movie in _movieSets[_testSetIndex])
                {
                    IEnumerable<WordDuplicated> currentMovieWords = _moviesWords.Where(word => word.MovieId == movie.MovieId);

                    foreach (WordDuplicated word in currentMovieWords)
                    {
                        _likelihoods.Add(CalculateLikelihood(ref shouldCalculateDenominator, category, movie.MovieId, word));
                    }
                }
            }
        }

        private IDictionary<MovieDuplicated, List<string>> ChooseCategoriesForMovies()
        {
            IDictionary<MovieDuplicated, List<string>> classifications = new Dictionary<MovieDuplicated, List<string>>();

            foreach (MovieDuplicated movie in _movieSets[_testSetIndex])
            {
                var categoriesLikelihoods = new Dictionary<string, double>();

                foreach (string category in _categories)
                {
                    double result = Math.Log10(_priors.FirstOrDefault(p => p.Category == category).Value);

                    IEnumerable<Likelihood> likelihoods = _likelihoods.Where(likelihood => likelihood.Category == category && likelihood.MovieId == movie.MovieId);

                    foreach (Likelihood likelihood in likelihoods)
                    {
                        double logarithm = Math.Log10(likelihood.Value);

                        result += likelihood.Occurred > 1 ? logarithm * likelihood.Occurred : logarithm;
                    }

                    categoriesLikelihoods.Add(category, result);
                }

                int categoriesCount = _originalMovies.Where(m => m.ImdbId == movie.ImdbId).Select(m => m.Genre).First().Split(',').Count();

                if (categoriesCount < 1)
                {
                    categoriesCount = 1;
                }

                List<string> outputCategories = categoriesLikelihoods
                    .OrderByDescending(cat => cat.Value)
                    .Take(categoriesCount)
                    .Select(cat => cat.Key)
                    .ToList();
                
                classifications.Add(movie, outputCategories);
            }

            return classifications;
        }

        private Likelihood CalculateLikelihood(ref bool shouldCalculateDenominator, string category, int movieId, WordDuplicated word)
        {
            if (shouldCalculateDenominator)
            {
                _denominator = GetAllWordsCountInTrainingSetForCategory(category) + GetUniqueWordsCountInTrainingSet();
                shouldCalculateDenominator = false;
            }

            return new Likelihood()
            {
                Category = category,
                MovieId = movieId,
                Value = (double)(GetWordOccuranceCountInTrainingSetForCategory(category, word.Text) + 1) / (_denominator),
                Occurred = word.Occurred,
                WordId = word.WordId
            };
        }

        private int GetAllWordsCountInTrainingSetForCategory(string category)
        {
            int allWordsCount = 0;

            for (int i = 0; i < _movieSets.Count; i++)
            {
                if (i == _testSetIndex)
                {
                    continue;
                }

                foreach (MovieDuplicated movie in _movieSets[i])
                {
                    if (movie.Genre != category)
                    {
                        continue;
                    }

                    IEnumerable<WordDuplicated> words = _moviesWords.Where(w => w.MovieId == movie.MovieId);

                    foreach (WordDuplicated word in words)
                    {
                        allWordsCount += word.Occurred;
                    }
                }
            }

            return allWordsCount;
        }

        private int GetWordOccuranceCountInTrainingSetForCategory(string category, string wordText)
        {
            int wordOccuranceCount = 0;

            for (int i = 0; i < _movieSets.Count; i++)
            {
                if (i == _testSetIndex)
                {
                    continue;
                }

                foreach (MovieDuplicated movie in _movieSets[i].Where(m => m.Genre == category))
                {
                    WordDuplicated word = _moviesWords.FirstOrDefault(w => w.MovieId == movie.MovieId && w.Text == wordText);

                    wordOccuranceCount += word != null ? word.Occurred : 0;
                }
            }

            return wordOccuranceCount;
        }

        private int GetUniqueWordsCountInTrainingSet()
        {
            var words = new Collection<WordDuplicated>();

            for (int i = 0; i < _movieSets.Count; i++)
            {
                if (i == _testSetIndex)
                {
                    continue;
                }

                foreach (MovieDuplicated movie in _movieSets[i])
                {
                    IEnumerable<WordDuplicated> movieWords = _moviesWords.Where(w => w.MovieId == movie.MovieId);

                    foreach (WordDuplicated word in movieWords)
                    {
                        if (words.Any(w => w.Text == word.Text) == false)
                        {
                            words.Add(word);
                        }
                    }
                }
            }

            return words.Count();
        }

        private string CleanString(string dirty)
        {
            string cleaned = new string(dirty.Where(c => !char.IsPunctuation(c)).ToArray()).ToLower();

            return Regex.Replace(cleaned, @"\s+", "");
        }
    }
}