﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Movies.Entities;
using Movies.Interfaces;
using Movies.Services;

namespace NaiveBayesClassifier.Data
{
    public static class DataPreparation
    {
        public static IEnumerable<Movie> GetMovies()
        {
            IMoviesService moviesService = new MoviesService();

            return moviesService.GetMovies(Settings.MoviesFromDBCount);
        }

        public static IEnumerable<Word> GetWordsByMovie(IEnumerable<Movie> movies)
        {
            IWordsService wordsService = new WordsService();

            return wordsService.GetWordsByMovie(movies);
        }

        public static ICollection<ICollection<Movie>> PrepareSets(IList<Movie> movies)
        {
            var movieSets = new Collection<ICollection<Movie>>();

            for (int setIndex = 0, movieIndex = 0; setIndex < Settings.SetsCount; setIndex++)
            {
                movieSets.Add(new Collection<Movie>());

                for (int i = 0; i < Settings.SetSize; i++, movieIndex++)
                {
                    movieSets[setIndex].Add(movies[movieIndex]);
                }
            }

            return movieSets;
        }        
    }
}
