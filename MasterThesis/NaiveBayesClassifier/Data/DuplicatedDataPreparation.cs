﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Movies.Entities;
using Movies.Services;

namespace NaiveBayesClassifier.Data
{
    public static class DuplicatedDataPreparation
    {
        public static IEnumerable<MovieDuplicated> GetMovies()
        {
            MoviesDuplicatedService moviesService = new MoviesDuplicatedService();

            return moviesService.GetMovies(Settings.MoviesFromDBCount);
        }

        public static IEnumerable<WordDuplicated> GetWordsByMovie(IEnumerable<MovieDuplicated> movies)
        {
            WordsDuplicatedService wordsService = new WordsDuplicatedService();

            return wordsService.GetWordsByMovie(movies);
        }

        public static ICollection<ICollection<MovieDuplicated>> PrepareSets(IList<MovieDuplicated> movies)
        {
            var movieSets = new Collection<ICollection<MovieDuplicated>>();

            for (int setIndex = 0, movieIndex = 0; setIndex < Settings.SetsCount; setIndex++)
            {
                movieSets.Add(new Collection<MovieDuplicated>());

                for (int i = 0; i < Settings.SetSize; i++, movieIndex++)
                {
                    movieSets[setIndex].Add(movies[movieIndex]);
                }
            }

            return movieSets;
        }        
    }
}
