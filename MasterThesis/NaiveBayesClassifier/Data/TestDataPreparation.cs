﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Movies.Entities;

namespace NaiveBayesClassifier.Data
{
    public static class TestDataPreparation
    {
        public static IEnumerable<Movie> GetMovies()
        {
            var movies = new Collection<Movie>();

            movies.Add(new Movie()
            {
                Genre = "c",
                MovieId = 5,
                Title = "Chinese Chinese Chinese Tokyo Japan"
            });

            movies.Add(new Movie()
            {
                Genre = "c",
                MovieId = 1,
                Title = "Chinese Beijing Chinese"
            });

            movies.Add(new Movie()
            {
                Genre = "c",
                MovieId = 2,
                Title = "Chinese Chinese Shanghai"
            });

            movies.Add(new Movie()
            {
                Genre = "c",
                MovieId = 3,
                Title = "Chinese Macao"
            });

            movies.Add(new Movie()
            {
                Genre = "j",
                MovieId = 4,
                Title = "Tokyo Japan Chinese"
            });

            return movies;
        }

        public static IEnumerable<Word> GetWordsByMovie()
        {
            var movieWords = new Collection<Word>();

            movieWords.Add(new Word()
            {
                MovieId = 1,
                Occurred = 2,
                Text = "Chinese",
                WordId = 1
            });

            movieWords.Add(new Word()
            {
                MovieId = 1,
                Occurred = 1,
                Text = "Beijing",
                WordId = 2
            });

            movieWords.Add(new Word()
            {
                MovieId = 2,
                Occurred = 2,
                Text = "Chinese",
                WordId = 3
            });

            movieWords.Add(new Word()
            {
                MovieId = 2,
                Occurred = 1,
                Text = "Shanghai",
                WordId = 4
            });

            movieWords.Add(new Word()
            {
                MovieId = 3,
                Occurred = 1,
                Text = "Chinese",
                WordId = 5
            });

            movieWords.Add(new Word()
            {
                MovieId = 3,
                Occurred = 1,
                Text = "Macao",
                WordId = 6
            });

            movieWords.Add(new Word()
            {
                MovieId = 4,
                Occurred = 1,
                Text = "Chinese",
                WordId = 7
            });

            movieWords.Add(new Word()
            {
                MovieId = 4,
                Occurred = 1,
                Text = "Tokyo",
                WordId = 8
            });

            movieWords.Add(new Word()
            {
                MovieId = 4,
                Occurred = 1,
                Text = "Japan",
                WordId = 9
            });

            movieWords.Add(new Word()
            {
                MovieId = 5,
                Occurred = 3,
                Text = "Chinese",
                WordId = 10
            });

            movieWords.Add(new Word()
            {
                MovieId = 5,
                Occurred = 1,
                Text = "Japan",
                WordId = 11
            });

            movieWords.Add(new Word()
            {
                MovieId = 5,
                Occurred = 1,
                Text = "Tokyo",
                WordId = 12
            });

            return movieWords;
        }

        public static ICollection<ICollection<Movie>> PrepareSets(IList<Movie> movies)
        {
            var sets = new Collection<ICollection<Movie>>();

            var movieSets = new Collection<ICollection<Movie>>();

            movieSets.Add(new Collection<Movie>());

            movieSets[0].Add(movies.First(m => m.MovieId == 5));

            movieSets.Add(new Collection<Movie>());

            movieSets[1].Add(movies.First(m => m.MovieId == 1));
            movieSets[1].Add(movies.First(m => m.MovieId == 2));
            movieSets[1].Add(movies.First(m => m.MovieId == 3));
            movieSets[1].Add(movies.First(m => m.MovieId == 4));

            return movieSets;
        }
    }
}
