﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using Movies.Entities;
using NaiveBayesClassifier.Model;

namespace NaiveBayesClassifier
{
    // TP - comedy is comedy
    // FP - classifier says it is action, but in real it is comedy
    // FN - classifier says it is not comedy, but in real it is comedy
    // TP - classifier says it is not comedy, and it is not comedy
    public static class Effectiveness
    {
        public static IDictionary<string, ConfusionMatrix> CalculateConfusionMatrix(IDictionary<Movie, string> classifications)
        {
            var confusionMatrix = new Dictionary<string, ConfusionMatrix>();

            foreach (string category in classifications.Values.Distinct())
            {
                if (confusionMatrix.Any(d => d.Key == category) == false)
                {
                    confusionMatrix.Add(category, new ConfusionMatrix());
                }

                IEnumerable<Movie> currentCategoryMovies = classifications.Where(c => c.Key.Genre == category).Select(m => m.Key);
                IEnumerable<Movie> moviesInCurrentTestSetExceptCurrentCategory = classifications.Where(c => c.Key.Genre != category).Select(m => m.Key);
                IEnumerable<Movie> correctlyGuessedCategories = classifications.Where(c => c.Value == category).Select(c => c.Key);
                IEnumerable<Movie> classificationsOtherThanCategory = classifications.Where(c => c.Value != category).Select(c => c.Key);

                confusionMatrix[category].TP = currentCategoryMovies.Intersect(correctlyGuessedCategories).Count();
                confusionMatrix[category].FN = currentCategoryMovies.Except(correctlyGuessedCategories).Count();
                confusionMatrix[category].FP = correctlyGuessedCategories.Except(currentCategoryMovies).Count();
                confusionMatrix[category].TN = moviesInCurrentTestSetExceptCurrentCategory.Intersect(classificationsOtherThanCategory).Count();
            }

            return confusionMatrix;
        }

        public static IDictionary<string, ClassifierSetEffectiveness> GetSetEffectiveness(IDictionary<string, ConfusionMatrix> confusionMatrix)
        {
            var cse = new Dictionary<string, ClassifierSetEffectiveness>();

            foreach (var cm in confusionMatrix)
            {
                if (cse.Any(d => d.Key == cm.Key) == false)
                {
                    cse.Add(cm.Key, new ClassifierSetEffectiveness());
                }

                double accuracyNumerator = cm.Value.TP + cm.Value.FN;
                double accuracyDenominator = Convert.ToDouble(cm.Value.TP + cm.Value.FP + cm.Value.FN + cm.Value.TN);
                cse[cm.Key].Accuracy = accuracyNumerator == 0 || accuracyDenominator == 0 ? 0 : accuracyNumerator / accuracyDenominator; ;

                double precisionNumerator = cm.Value.TP;
                double precisionDenominator = Convert.ToDouble(cm.Value.TP + cm.Value.FP);
                cse[cm.Key].Precision = precisionNumerator == 0 || precisionDenominator == 0 ? 0 : precisionNumerator / precisionDenominator; ;

                double recallNumerator = cm.Value.TP;
                double recallDenominator = Convert.ToDouble(cm.Value.TP + cm.Value.FN);
                cse[cm.Key].Recall = recallNumerator == 0 || recallDenominator == 0 ? 0 : recallNumerator / recallDenominator; ;

                double falloutNumerator = cm.Value.FP;
                double falloutDenominator = Convert.ToDouble(cm.Value.FP + cm.Value.TN);
                cse[cm.Key].Fallout = falloutNumerator == 0 || falloutDenominator == 0 ? 0 : falloutNumerator / falloutDenominator;

                double specificityNumerator = cm.Value.TN;
                double specificityDenominator = Convert.ToDouble(cm.Value.FP + cm.Value.TN);
                cse[cm.Key].Specificity = specificityNumerator == 0 || specificityDenominator == 0 ? 0 : specificityNumerator / specificityDenominator;

                double f1ScoreNumerator = (2 * cse[cm.Key].Precision * cse[cm.Key].Recall);
                double f1ScoreDenominator = (cse[cm.Key].Precision + cse[cm.Key].Recall);
                cse[cm.Key].FMeasure = f1ScoreNumerator == 0 || f1ScoreDenominator == 0 ? 0 : f1ScoreNumerator / f1ScoreDenominator;
            }

            return cse;
        }

        public static ICollection<KeyValuePair<string, ClassifierSetEffectiveness>> CalculateClassifierEffectiveness(ICollection<IDictionary<string, ClassifierSetEffectiveness>> setsEffectivenesses)
        {
            var result = new Collection<KeyValuePair<string, ClassifierSetEffectiveness>>();
            var collectedClassification = new Collection<KeyValuePair<string, ICollection<ClassifierSetEffectiveness>>>();

            foreach (Dictionary<string, ClassifierSetEffectiveness> set in setsEffectivenesses)
            {
                foreach (var s in set)
                {
                    if (collectedClassification.Any(r => r.Key == s.Key) == false)
                    {
                        var setEffectiveness = new Collection<ClassifierSetEffectiveness>();
                        setEffectiveness.Add(s.Value);
                        var pair = new KeyValuePair<string, ICollection<ClassifierSetEffectiveness>>(s.Key, setEffectiveness);

                        collectedClassification.Add(pair);
                    }
                    else
                    {
                        collectedClassification.Where(r => r.Key == s.Key).First().Value.Add(s.Value);
                    }
                }
            }

            foreach (var classification in collectedClassification)
            {
                var classifierTotalEffectiveness = new ClassifierSetEffectiveness();

                foreach (var cse in classification.Value)
                {
                    classifierTotalEffectiveness.Accuracy += cse.Accuracy;
                    classifierTotalEffectiveness.Precision += cse.Precision;
                    classifierTotalEffectiveness.Fallout += cse.Fallout;
                    classifierTotalEffectiveness.FMeasure += cse.FMeasure;
                    classifierTotalEffectiveness.Recall += cse.Recall;
                    classifierTotalEffectiveness.Specificity += cse.Specificity;
                }

                int classificationsCount = classification.Value.Count();

                classifierTotalEffectiveness.Accuracy /= classificationsCount;
                classifierTotalEffectiveness.Precision /= classificationsCount;
                classifierTotalEffectiveness.Fallout /= classificationsCount;
                classifierTotalEffectiveness.FMeasure /= classificationsCount;
                classifierTotalEffectiveness.Recall /= classificationsCount;
                classifierTotalEffectiveness.Specificity /= classificationsCount;

                result.Add(new KeyValuePair<string, ClassifierSetEffectiveness>(classification.Key, classifierTotalEffectiveness));
            }

            return result;
        }
        
        public static double CheckOverallClassifierCorrectness(IDictionary<Movie, string> classifications)
        {
            int correctlyClassified = 0;

            foreach (KeyValuePair<Movie, string> classification in classifications)
            {
                if (classification.Key.Genre == classification.Value)
                {
                    correctlyClassified++;
                }
            }

            double result = ((double)correctlyClassified / classifications.Count()) * 100;

            return result;
        }

        [Obsolete]
        public static Tuple<double, double> CheckClassifierCorrectnessInOptymisticWay(IDictionary<Movie, string> classifications)
        {
            int totalCategories = 0;
            int correctlyClassified = 0;
            bool shouldBreak = false;

            foreach (KeyValuePair<Movie, string> classification in classifications)
            {
                string[] splittedDefaultCategories = classification.Key.Genre.Split(',');
                string[] splittedClassifiedCategories = classification.Value.Split(',');

                foreach (string defaultCat in splittedDefaultCategories)
                {
                    string defaultCatCleaned = CleanString(defaultCat);

                    foreach (string classifiedCat in splittedClassifiedCategories)
                    {
                        totalCategories++;

                        string classifiedCatCleaned = CleanString(classifiedCat);

                        if (defaultCatCleaned == classifiedCatCleaned)
                        {
                            correctlyClassified++;
                            shouldBreak = true;
                            break;
                        }
                    }

                    if (shouldBreak)
                    {
                        shouldBreak = false;
                        break;
                    }
                }
            }

            double result = ((double)correctlyClassified / classifications.Count()) * 100;
            double proportions = ((double)correctlyClassified / totalCategories) * 100;

            return new Tuple<double, double>(result, proportions);
        }

        [Obsolete]
        private static string CleanString(string dirty)
        {
            string cleaned = new string(dirty.Where(c => !char.IsPunctuation(c)).ToArray()).ToLower();

            return Regex.Replace(cleaned, @"\s+", "");
        }
    }
}
