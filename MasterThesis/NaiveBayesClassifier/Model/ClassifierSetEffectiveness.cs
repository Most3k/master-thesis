﻿namespace NaiveBayesClassifier.Model
{
    public class ClassifierSetEffectiveness
    {
        public double Accuracy { get; set; }

        public double Precision { get; set; }

        public double Recall { get; set; }

        public double Fallout { get; set; }

        public double Specificity { get; set; }

        public double FMeasure { get; set; }

        public double OneCategoryMatchPercent { get; set; }

        public double ProportionsMatchPercent { get; set; }
    }
}
