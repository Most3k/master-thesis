﻿namespace NaiveBayesClassifier.Model
{
    public class ConfusionMatrix
    {
        public int TP { get; set; }

        public int FP { get; set; }

        public int FN { get; set; }

        public int TN { get; set; }
    }
}
