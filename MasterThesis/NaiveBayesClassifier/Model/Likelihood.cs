﻿namespace NaiveBayesClassifier.Model
{
    public class Likelihood
    {
        public int MovieId { get; set; }

        public int WordId { get; set; }

        public double Value { get; set; }

        public int Occurred { get; set; }

        public string Category { get; set; }
    }
}
