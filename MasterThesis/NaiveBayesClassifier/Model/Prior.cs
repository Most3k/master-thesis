﻿namespace NaiveBayesClassifier.Model
{
    public class Prior
    {
        public string Category { get; set; }

        public double Value { get; set; }
    }
}
