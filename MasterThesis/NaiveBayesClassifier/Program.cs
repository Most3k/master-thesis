﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Movies;
using Movies.Entities;
using NaiveBayesClassifier.Data;
using NaiveBayesClassifier.Model;

namespace NaiveBayesClassifier
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Test data
            //IEnumerable<Movie> movies = TestDataPreparation.GetMovies();
            //IEnumerable<Word> movieWords = TestDataPreparation.GetWordsByMovie();
            //ICollection<ICollection<Movie>> movieSets = TestDataPreparation.PrepareSets(movies.ToList());
            #endregion

            #region Experiment #1
            IEnumerable<Movie> movies = DataPreparation.GetMovies();
            Statistic.SaveMoviesCategoriesCountToFile(movies);

            IEnumerable<Word> movieWords = DataPreparation.GetWordsByMovie(movies);
            ICollection<ICollection<Movie>> movieSets = DataPreparation.PrepareSets(movies.ToList());

            var classifier = new Classifier(movies.ToList(), movieWords.ToList(), movieSets.ToList());
            ICollection<KeyValuePair<string, ClassifierSetEffectiveness>> classifierEffectiveness = classifier.Classificate();

            Statistic.SaveClassifierEffectivenessToFile(classifierEffectiveness);
            #endregion Experiment #1

            #region Experiment #3
            //IEnumerable<MovieDuplicated> movies = DuplicatedDataPreparation.GetMovies();
            //Statistic.SaveMoviesCategoriesCountToFile(movies);

            //IEnumerable<WordDuplicated> movieWords = DuplicatedDataPreparation.GetWordsByMovie(movies);
            //ICollection<ICollection<MovieDuplicated>> movieSets = DuplicatedDataPreparation.PrepareSets(movies.ToList());

            //var classifier = new ClassifierDuplicated(movies.ToList(), movieWords.ToList(), movieSets.ToList());
            //ICollection<KeyValuePair<string, ClassifierSetEffectiveness>> classifierEffectiveness = classifier.Classificate();

            //Statistic.SaveClassifierEffectivenessToFile(classifierEffectiveness);
            #endregion Experiment #3
        }
    }
}
