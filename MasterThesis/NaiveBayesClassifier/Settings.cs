﻿using System.IO;

namespace NaiveBayesClassifier
{
    public static class Settings
    {
        #region Test data config
        //public static int MoviesFromDBCount = 5;

        //public static int SetsCount = 2;

        //public static int SetSize = 4;

        //public static int TrainingSetMoviesCount = 4;
        #endregion Test data config

        public static int MoviesFromDBCount = 250;

        public static int SetsCount = 10;

        public static int SetSize = MoviesFromDBCount / SetsCount;

        public static int TrainingSetMoviesCount = MoviesFromDBCount - 1;

        public static string ExperimentFolderPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\Experiments\\";

        public static string ExperimentsFilePrefix = "Exp";

        public static string StatisticsFilePrefix = "Sta";
    }
}