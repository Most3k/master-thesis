﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Movies.Entities;
using NaiveBayesClassifier.Model;

namespace NaiveBayesClassifier
{
    public static class Statistic
    {
        public static void SaveClassifierEffectivenessToFile(ICollection<KeyValuePair<string, ClassifierSetEffectiveness>> ce)
        {
            string date = DateTime.Now.ToString("yyyy_MM_dd_hh_mm");
            string fileName = $"{Settings.ExperimentsFilePrefix}_{Settings.MoviesFromDBCount}_{Settings.SetsCount}_{date}.csv";
            string fullFilePath = Settings.ExperimentFolderPath + fileName;

            using (StreamWriter writer = File.AppendText(fullFilePath))
            {
                string header = "Kategoria filmu;Dokladnosc;Precyzja;Specyficznosc;Czulosc;Fall-out;Pomiar F";
                writer.WriteLine(header);

                foreach (KeyValuePair<string, ClassifierSetEffectiveness> item in ce)
                {
                    string lineToSave = $"{item.Key};{item.Value.Accuracy};{item.Value.Precision};{item.Value.Specificity};{item.Value.Recall};{item.Value.Fallout};{item.Value.FMeasure}";

                    writer.WriteLine(lineToSave);
                }
            }
        }

        public static void SaveMoviesCategoriesCountToFile(IEnumerable<Movie> movies)
        {
            string date = DateTime.Now.ToString("yyyy_MM_dd_hh_mm");
            string fileName = $"{Settings.StatisticsFilePrefix}_{Settings.MoviesFromDBCount}_{Settings.SetsCount}_{date}.csv";
            string fullFilePath = Settings.ExperimentFolderPath + fileName;

            using (StreamWriter writer = File.AppendText(fullFilePath))
            {
                string header = "Kategoria filmu;Liczba filmow";
                writer.WriteLine(header);

                foreach (IGrouping<string, Movie> group in movies.GroupBy(m => m.Genre).OrderByDescending(o => o.Count()))
                {
                    string lineToSave = $"{group.Key};{group.Count()}";

                    writer.WriteLine(lineToSave);
                }
            }
        }

        public static void SaveMoviesCategoriesCountToFile(IEnumerable<MovieDuplicated> movies)
        {
            string date = DateTime.Now.ToString("yyyy_MM_dd_hh_mm");
            string fileName = $"{Settings.StatisticsFilePrefix}_{Settings.MoviesFromDBCount}_{Settings.SetsCount}_{date}.csv";
            string fullFilePath = Settings.ExperimentFolderPath + fileName;

            using (StreamWriter writer = File.AppendText(fullFilePath))
            {
                string header = "Kategoria filmu;Liczba filmow";
                writer.WriteLine(header);

                foreach (IGrouping<string, MovieDuplicated> group in movies.GroupBy(m => m.Genre).OrderByDescending(o => o.Count()))
                {
                    string lineToSave = $"{group.Key};{group.Count()}";

                    writer.WriteLine(lineToSave);
                }
            }
        }
    }
}
